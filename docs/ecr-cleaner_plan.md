## ecr-cleaner plan

Plan will show the effect of the lifecycle policies without applying them.

### Synopsis

Plan will show the effect of the lifecycle policies without applying them. If no filepath is specified ~/policies will be used as default.

```
ecr-cleaner plan [filepath1 filepath2 ...] [flags]
```

### Options

```
  -h, --help      help for plan
  -v, --verbose   set to get a complete list of images and their actions in the plan
```

### Options inherited from parent commands

```
  -o, --output string   type of output, either yaml or json (default "yaml")
  -r, --region string   Region to use in the aws account (default "us-east-1")
```

### SEE ALSO

* [ecr-cleaner](ecr-cleaner.md)	 - This utility allows to clean ecr repos based in more comprehensive policies to expire images

###### Auto generated by spf13/cobra on 21-Dec-2020
