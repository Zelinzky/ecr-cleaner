## ecr-cleaner

This utility allows to clean ecr repos based in more comprehensive policies to expire images

### Synopsis

aws ECR expiration policies are limited in several ways, don't have flexible selectors, can only use prefixes for tag selection and only are able to use on action "expire".
This utility aims to offer more comprehensive expiration/lifecycle policies to work with aws ECR. This mean flexible selectors and the ability to create complex policies using either JSON or YAML

### Options

```
  -h, --help            help for ecr-cleaner
  -o, --output string   type of output, either yaml or json (default "yaml")
  -r, --region string   Region to use in the aws account (default "us-east-1")
```

### SEE ALSO

* [ecr-cleaner apply](ecr-cleaner_apply.md)	 - apply will clean ecr using the given policies.
* [ecr-cleaner doc](ecr-cleaner_doc.md)	 - generates utility documentation in given path
* [ecr-cleaner get](ecr-cleaner_get.md)	 - Get resource in a region
* [ecr-cleaner plan](ecr-cleaner_plan.md)	 - Plan will show the effect of the lifecycle policies without applying them.
* [ecr-cleaner policy](ecr-cleaner_policy.md)	 - policy related operations

###### Auto generated by spf13/cobra on 21-Dec-2020
