package commands

import (
	"ecr-cleaner/internal/aws"
	"ecr-cleaner/internal/io"
	"ecr-cleaner/internal/policies"
	"fmt"
	"sort"

	"github.com/spf13/viper"

	"github.com/spf13/cobra"
)

func newPlanCommand() *cobra.Command {
	cmd := cobra.Command{
		Use:   "plan [filepath1 filepath2 ...]",
		Short: "Plan will show the effect of the lifecycle policies without applying them.",
		Long:  "Plan will show the effect of the lifecycle policies without applying them. If no filepath is specified ~/policies will be used as default.",
		RunE: func(cmd *cobra.Command, args []string) error {
			if len(args) == 0 {
				fmt.Printf("no directory or file was specified, default directory(%s) will be used\n", defaultPolicyDir)
				args = append(args, defaultPolicyDir)
			}
			_, err := runPlan(args)
			if err != nil {
				return fmt.Errorf("error running plan: %w", err)
			}
			return nil
		},
	}

	cmd.Flags().BoolP("verbose", "v", false, "set to get a complete list of images and their actions in the plan")
	_ = viper.BindPFlag("verbose", cmd.Flags().Lookup("verbose"))

	return &cmd
}

func runPlan(filePaths []string) ([]aws.ImgOutput, error) {
	readPolicies, err := readPoliciesMultipleDir(filePaths)
	if err != nil {
		return []aws.ImgOutput{}, fmt.Errorf("error reading policies: %w", err)
	}
	err = policies.CheckPolicies(readPolicies, false)
	if err != nil {
		return []aws.ImgOutput{}, fmt.Errorf("error found in the policies: %w", err)
	}

	var plannedImages []aws.ImgOutput
	for _, policy := range readPolicies {
		imagesPerRepo, err := getImages(policy.Repositories)
		if err != nil {
			return []aws.ImgOutput{}, fmt.Errorf("error getting images for policy %s: %w", policy.Name, err)
		}
		plannedImages = append(plannedImages, planImages(policy.Rules, imagesPerRepo)...)
	}

	err = PrintPlan(plannedImages, viper.GetBool("verbose"))
	if err != nil {
		return nil, err
	}
	return plannedImages, nil
}

func planImages(rules []policies.Rule, imagesArr []aws.ImgOutput) []aws.ImgOutput {
	sort.Slice(rules, func(i, j int) bool {
		return rules[i].RulePriority < rules[j].RulePriority
	})
	proc := make(chan aws.ImgOutput, len(imagesArr))

	planWorker := func(images aws.ImgOutput, rulesWorker []policies.Rule, out chan aws.ImgOutput) {
		go func() {
			for _, rule := range rulesWorker {
				for i, image := range images.Images {
					image.ApplyRule(rule, i)
					images.Images[i] = image
				}
			}
			out <- images
		}()
	}
	for _, images := range imagesArr {
		planWorker(images, rules, proc)
	}
	var imagesProcessed []aws.ImgOutput
	for i := 0; i < len(imagesArr); i++ {
		images := <-proc
		imagesProcessed = append(imagesProcessed, images)
	}
	return imagesProcessed
}

func PrintPlan(plan []aws.ImgOutput, verbose bool) error {
	totalImgKept := 0
	totalImgExpired := 0
	totalImgUnaltered := 0
	for _, repo := range plan {
		if verbose {
			fmt.Println("Repository: ", repo.EcrName)
		}
		imgKeptInRepo := 0
		imgExpiredInRepo := 0
		imgUnalteredInRepo := 0
		for _, image := range repo.Images {
			imageBytes, err := io.SPrintObject(image)
			if err != nil {
				return fmt.Errorf("error printing object: %w", err)
			}
			switch image.Action {
			case "expire":
				if verbose {
					fmt.Println(io.Red(string(imageBytes)))
				}
				imgExpiredInRepo++
			case "keep":
				if verbose {
					fmt.Println(io.Green(string(imageBytes)))
				}
				imgKeptInRepo++
			default:
				if verbose {
					fmt.Println(string(imageBytes))
				}
				imgUnalteredInRepo++
			}
		}
		fmt.Printf("In repo: %s\n", repo.EcrName)
		fmt.Printf(io.Green("\t%d images are being kept explicitly.\n"), imgKeptInRepo)
		fmt.Printf("\t%d images are being kept implicitly.\n", imgUnalteredInRepo)
		fmt.Printf(io.Red("\t%d images are being expired.\n"), imgExpiredInRepo)
		totalImgKept += imgKeptInRepo
		totalImgExpired += imgExpiredInRepo
		totalImgUnaltered += imgUnalteredInRepo
	}

	fmt.Printf("In total:\n")
	fmt.Printf(io.Green("\t%d images are being kept explicitly.\n"), totalImgKept)
	fmt.Printf("\t%d images are being kept implicitly.\n", totalImgUnaltered)
	fmt.Printf(io.Red("\t%d images are being expired.\n"), totalImgExpired)
	return nil
}
