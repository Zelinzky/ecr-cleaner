package commands

import (
	"ecr-cleaner/internal/aws"
	"ecr-cleaner/internal/io"
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func newGetCommand() *cobra.Command {
	cmd := cobra.Command{
		Use:   "get <images|ecr>",
		Short: "Get resource in a region",
	}

	cmd.AddCommand(newGetImagesCommand())
	cmd.AddCommand(newGetEcrCommand())

	cmd.PersistentFlags().Bool("printARN", false, "include ARN of the resource in the output")
	_ = viper.BindPFlag("printARN", cmd.PersistentFlags().Lookup("printARN"))
	cmd.PersistentFlags().Bool("printURI", false, "include URI of the resource in the output")
	_ = viper.BindPFlag("printURI", cmd.PersistentFlags().Lookup("printURI"))

	return &cmd
}

func newGetEcrCommand() *cobra.Command {
	cmd := cobra.Command{
		Use:   "ecr",
		Short: "Lists all ecr in a region.",
		Args:  cobra.ExactArgs(0),
		RunE: func(cmd *cobra.Command, args []string) error {
			if err := runGetEcr(); err != nil {
				return fmt.Errorf("run get ecr: %w", err)
			}
			return nil
		},
	}

	return &cmd
}

func runGetEcr() error {
	list, err := aws.ListRepositories(viper.GetString("region"), viper.GetBool("printARN"), viper.GetBool("printURI"))
	if err != nil {
		return err
	}

	_, err = io.PrintObject(list)
	if err != nil {
		return err
	}

	return nil
}

func newGetImagesCommand() *cobra.Command {
	cmd := cobra.Command{
		Use:   "images repository_name1 repository_name2...",
		Short: "Lists all images in an ecr.",
		Args:  cobra.MinimumNArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			if err := runGetImages(args); err != nil {
				return fmt.Errorf("run get images: %w", err)
			}
			return nil
		},
	}

	return &cmd
}

func runGetImages(repositories []string) error {
	imagesList, err := getImages(repositories)
	if err != nil {
		return err
	}

	_, err = io.PrintObject(imagesList)
	if err != nil {
		return err
	}

	return nil
}

func getImages(repositories []string) ([]aws.ImgOutput, error) {
	var imagesList []aws.ImgOutput
	for _, ecrName := range repositories {
		images, err := aws.ListImages(ecrName, viper.GetString("region"))
		if err != nil {
			return []aws.ImgOutput{}, fmt.Errorf("error getting images from %s repository: %w", ecrName, err)
		}
		imagesList = append(imagesList, aws.ImgOutput{
			EcrName: ecrName,
			Images:  images,
		})
	}
	return imagesList, nil
}
