package commands

import (
	"ecr-cleaner/internal/io"
	"ecr-cleaner/internal/policies"
	"fmt"
	"io/ioutil"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const defaultPolicyDir = "~/policies"

func newPolicyCommand() *cobra.Command {
	cmd := cobra.Command{
		Use:   "policy <check|example>",
		Short: "policy related operations",
	}

	cmd.AddCommand(newExampleCommand())
	cmd.AddCommand(newPolicyCheckCommand())

	return &cmd
}

func newExampleCommand() *cobra.Command {
	cmd := cobra.Command{
		Use:   "example [-o json|yaml] [-F filename]",
		Short: "Prints example policy",
		Long:  "Prints example policy to standard out, to create an example file use flag -F",
		RunE: func(cmd *cobra.Command, args []string) error {
			err := runPolicyExample()
			if err != nil {
				return fmt.Errorf("failed to create example: %w", err)
			}
			return nil
		},
	}

	cmd.Flags().StringP("filename", "F", "", "name of the output file")
	_ = viper.BindPFlag("filename", cmd.Flags().Lookup("filename"))

	return &cmd
}

func runPolicyExample() error {
	policy := policies.GenerateExamplePolicy()
	out := []policies.Policy{policy, policy}
	outBytes, err := io.PrintObject(out)
	if err != nil {
		return fmt.Errorf("error writing to stdout: %w", err)
	}

	if viper.GetString("filename") != "" {
		err = ioutil.WriteFile(viper.GetString("filename"), outBytes, 0644)
		if err != nil {
			return fmt.Errorf("error writing to file: %w", err)
		}
	}

	return nil
}

func newPolicyCheckCommand() *cobra.Command {
	cmd := cobra.Command{
		Use:   "check [filepath1 filepath2 ...]",
		Short: fmt.Sprintf("check will check if a policy file or files in a directory are valid policies. Default policy directory is %s", defaultPolicyDir),
		RunE: func(cmd *cobra.Command, args []string) error {
			if len(args) == 0 {
				fmt.Printf("no directory or file was specified, default directory(%s) will be used\n", defaultPolicyDir)
				args = append(args, defaultPolicyDir)
			}
			err := runPolicyCheck(args)
			if err != nil {
				return fmt.Errorf("error running policy check: %w", err)
			}
			return nil
		},
	}

	return &cmd
}

func runPolicyCheck(filePaths []string) error {
	policyList, err2 := readPoliciesMultipleDir(filePaths)
	if err2 != nil {
		return err2
	}
	err := policies.CheckPolicies(policyList, true)
	if err != nil {
		fmt.Printf(io.Red("error found in the policies: %s\n"), err.Error())
		return nil
	}
	fmt.Println(io.Green("\nAll policies seem to be OK."))
	return nil
}

func readPoliciesMultipleDir(filePaths []string) ([]policies.Policy, error) {
	var policyList []policies.Policy
	for _, filePath := range filePaths {
		argPolicy, err := policies.ReadPoliciesFromDir(filePath)
		if err != nil {
			return nil, fmt.Errorf("error reading the policies: %w", err)
		}
		policyList = append(policyList, argPolicy...)
	}
	return policyList, nil
}
