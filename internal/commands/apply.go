package commands

import (
	"ecr-cleaner/internal/aws"
	"ecr-cleaner/internal/io"
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func newApplyCommand() *cobra.Command {
	cmd := cobra.Command{
		Use:   "apply [filepath1 filepath2 ...] [-y]",
		Short: "apply will clean ecr using the given policies.",
		Long:  "apply will clean ecr using the given policies. If no filepath is specified ~/policies will be used as default.",
		RunE: func(cmd *cobra.Command, args []string) error {
			if len(args) == 0 {
				fmt.Printf("no directory or file was specified, default directory(%s) will be used\n", defaultPolicyDir)
				args = append(args, defaultPolicyDir)
			}
			err := runApply(args)
			if err != nil {
				return fmt.Errorf("error applying policies: %w", err)
			}
			return nil
		},
	}
	cmd.Flags().BoolP("verbose", "v", false, "set to get a complete list of images and their actions in the plan")
	_ = viper.BindPFlag("verbose", cmd.Flags().Lookup("verbose"))

	cmd.Flags().BoolP("auto-approve", "y", false, "skip interactive approval of plan before applying")
	_ = viper.BindPFlag("auto-approve", cmd.Flags().Lookup("auto-approve"))

	return &cmd
}

func runApply(filePaths []string) error {
	plannedImages, err := runPlan(filePaths)
	if err != nil {
		return fmt.Errorf("error planing the policy apply: %w", err)
	}
	if !viper.GetBool("auto-approve") {
		fmt.Print(`Do you want to perform these actions?
  Only 'yes' will be accepted to approve.
Enter a value: `)
		var answer string
		_, err = fmt.Scanln(&answer)
		if err != nil {
			return fmt.Errorf("error reading stdin: %w", err)
		}
		if answer != "yes" {
			println(io.Red("\nThe command was aborted."))
			return nil
		}
	}
	for _, imgOutput := range plannedImages {
		expiredImages := aws.GetExpired(imgOutput.Images)

		//this loop is needed because the aws api can only accept up to 100 image ids per batch deletion requests
		for i := 0; i < len(expiredImages); i+=100 {
			var imagesSection []aws.Image

			if i+100 >= len(expiredImages){
				imagesSection = expiredImages[i:]
			} else {
				imagesSection = expiredImages[i:i+100]
			}
			deleteImageInput := aws.GenerateDeleteImageInput(imgOutput.EcrName, imagesSection)
			if viper.GetBool("verbose") {
				fmt.Println("Delete request:")
				fmt.Println(deleteImageInput)
			}
			fmt.Printf(io.Purple("Deleting images from %s . . .\n"), imgOutput.EcrName)
			failures, err := aws.DeleteImagesFromRepo(viper.GetString("region"), deleteImageInput)
			if err != nil {
				return fmt.Errorf("error deleting images: %w", err)
			}
			if len(failures) > 0 {
				_, err := io.PrintObject(failures)
				if err != nil {
					return fmt.Errorf("error printing failures: %w", err)
				}
			}
		}
	}
	fmt.Println("finished")
	return nil
}
