package commands

import (
	"os"
	"path"

	"github.com/spf13/viper"

	"github.com/spf13/cobra"
)

func NewDefaultCommand() *cobra.Command {

	cmd := cobra.Command{
		Use:   path.Base(os.Args[0]),
		Short: "This utility allows to clean ecr repos based in more comprehensive policies to expire images",
		Long: `aws ECR expiration policies are limited in several ways, don't have flexible selectors, can only use prefixes for tag selection and only are able to use on action "expire".
This utility aims to offer more comprehensive expiration/lifecycle policies to work with aws ECR. This mean flexible selectors and the ability to create complex policies using either JSON or YAML`,
	}

	cmd.PersistentFlags().StringP("region", "r", "us-east-1", "Region to use in the aws account")
	_ = viper.BindPFlag("region", cmd.PersistentFlags().Lookup("region"))

	cmd.PersistentFlags().StringP("output", "o", "yaml", "type of output, either yaml or json")
	_ = viper.BindPFlag("output", cmd.PersistentFlags().Lookup("output"))

	cmd.AddCommand(newGetCommand())
	cmd.AddCommand(newPolicyCommand())
	cmd.AddCommand(newPlanCommand())
	cmd.AddCommand(newApplyCommand())
	cmd.AddCommand(newDocCommand())

	return &cmd
}
