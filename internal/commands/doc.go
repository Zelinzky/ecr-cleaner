package commands

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/cobra/doc"
)

func newDocCommand() *cobra.Command {
	cmd := cobra.Command{
		Use:   "doc [path]",
		Short: "generates utility documentation in given path",
		Args:  cobra.MaximumNArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			dir := "./docs/"
			if len(args) == 1 {
				dir = args[0]
			}
			if _, err := os.Stat(dir); os.IsNotExist(err) {
				err := os.Mkdir(dir, os.ModeDir|0766)
				if err != nil {
					return fmt.Errorf("error creating doc directory: %w")
				}
			}
			err := doc.GenMarkdownTree(NewDefaultCommand(), dir)
			if err != nil {
				return fmt.Errorf("error creating documentation: %w", err)
			}
			return nil
		},
	}

	return &cmd
}
