package io

import (
	"encoding/json"
	"fmt"

	"github.com/spf13/viper"
	"gopkg.in/yaml.v2"
)

var (
	Black   = Color("\033[1;30m%s\033[0m")
	Red     = Color("\033[1;31m%s\033[0m")
	Green   = Color("\033[1;32m%s\033[0m")
	Yellow  = Color("\033[1;33m%s\033[0m")
	Purple  = Color("\033[1;34m%s\033[0m")
	Magenta = Color("\033[1;35m%s\033[0m")
	Teal    = Color("\033[1;36m%s\033[0m")
	White   = Color("\033[1;37m%s\033[0m")
)

func Color(colorString string) func(...interface{}) string {
	sprint := func(args ...interface{}) string {
		return fmt.Sprintf(colorString, fmt.Sprint(args...))
	}
	return sprint
}

func PrintObject(list interface{}) ([]byte, error) {
	outBytes, err := SPrintObject(list)
	if err != nil {
		return nil, err
	}
	outString := string(outBytes)
	fmt.Println(outString)
	return outBytes, nil
}

func SPrintObject(list interface{}) ([]byte, error) {
	switch viper.GetString("output") {
	case "yaml", "yml", "YAML", "YML":
		outBytes, err := yaml.Marshal(list)
		if err != nil {
			return []byte{}, fmt.Errorf("error marshaling yaml: %w", err)
		}
		return outBytes, nil
	case "json", "JSON":
		outBytes, err := json.MarshalIndent(list, "", "\t")
		if err != nil {
			return []byte{}, fmt.Errorf("error marshaling json: %w", err)
		}
		return outBytes, nil
	default:
		return []byte{}, fmt.Errorf("output flag value is invalid please check and try again")
	}
}
