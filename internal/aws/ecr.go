package aws

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ecr"
)

type Repository struct {
	Name string `json:"name"`
	Arn  string `yaml:",omitempty" json:"arn,omitempty"`
	Uri  string `yaml:",omitempty" json:"uri,omitempty"`
}

func getEcr(session *session.Session, nextToken *string) ([]*ecr.Repository, error) {
	ecrSvc := ecr.New(session)
	repositoryInput := &ecr.DescribeRepositoriesInput{NextToken: nextToken}

	ecrOutput, err := ecrSvc.DescribeRepositories(repositoryInput)
	if err != nil {
		return []*ecr.Repository{}, fmt.Errorf("error getting ecr list: %w", err)
	}

	repositories := ecrOutput.Repositories
	if ecrOutput.NextToken != nil {
		additionalRepos, err := getEcr(session, ecrOutput.NextToken)
		if err != nil {
			return []*ecr.Repository{}, err
		}
		repositories = append(repositories, additionalRepos...)
	}

	return repositories, nil
}

func ListRepositories(region string, printArn, printUri bool) ([]Repository, error) {
	sess, err := getSession(region)
	if err != nil {
		return []Repository{}, fmt.Errorf("error getting session: %w", err)
	}

	repoList, err := getEcr(sess, nil)
	if err != nil {
		return []Repository{}, fmt.Errorf("error getting repositories: %w", err)
	}

	var outList []Repository
	for _, repository := range repoList {
		name := *repository.RepositoryName
		data := Repository{Name: name}
		if printArn {
			arn := *repository.RepositoryArn
			data.Arn = arn
		}

		if printUri {
			uri := *repository.RepositoryUri
			data.Uri = uri
		}

		outList = append(outList, data)
	}

	return outList, nil
}
