package aws

import (
	"ecr-cleaner/internal/policies"
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ecr"
)

const day = 24 * time.Hour

type Image struct {
	Digest   string    `json:"digest"`
	PushedAt time.Time `yaml:"pushedAt" json:"pushedAt"`
	Tags     []*string `json:"tags"`
	Action   string    `yaml:",omitempty" json:",omitempty"`
}

type ImgOutput struct {
	EcrName string
	Images  []Image
}

func getImages(repositoryName string, session *session.Session, nextToken *string) ([]*ecr.ImageDetail, error) {
	ecrSvc := ecr.New(session)
	imagesInput := &ecr.DescribeImagesInput{NextToken: nextToken, RepositoryName: &repositoryName}
	imagesOutput, err := ecrSvc.DescribeImages(imagesInput)
	if err != nil {
		return []*ecr.ImageDetail{}, fmt.Errorf("error getting images list: %w", err)
	}

	imageDetail := imagesOutput.ImageDetails
	if imagesOutput.NextToken != nil {
		additionalImages, err := getImages(repositoryName, session, imagesOutput.NextToken)
		if err != nil {
			return []*ecr.ImageDetail{}, err
		}
		imageDetail = append(imageDetail, additionalImages...)
	}
	return imageDetail, nil

}

func DeleteImagesFromRepo(region string, imagesToDelete ecr.BatchDeleteImageInput) ([]*ecr.ImageFailure, error) {
	sess, err := getSession(region)
	ecrSvc := ecr.New(sess)
	batchDeleteImageOutput, err := ecrSvc.BatchDeleteImage(&imagesToDelete)
	if err != nil {
		return []*ecr.ImageFailure{}, fmt.Errorf("error deleting images: %w", err)
	}
	return batchDeleteImageOutput.Failures, nil
}

func GenerateDeleteImageInput(repositoryName string, imagesToDelete []Image) ecr.BatchDeleteImageInput {
	var deleteImageInput ecr.BatchDeleteImageInput
	deleteImageInput.SetRepositoryName(repositoryName)
	var imagesToDeleteIDs []*ecr.ImageIdentifier
	for _, image := range imagesToDelete {
		var imageID ecr.ImageIdentifier
		imageID.SetImageDigest(image.Digest)
		imagesToDeleteIDs = append(imagesToDeleteIDs, &imageID)
	}
	deleteImageInput.SetImageIds(imagesToDeleteIDs)
	return deleteImageInput
}

func ListImages(repositoryName, region string) ([]Image, error) {
	sess, err := getSession(region)
	if err != nil {
		return []Image{}, fmt.Errorf("error getting session: %w", err)
	}
	imageList, err := getImages(repositoryName, sess, nil)
	if err != nil {
		return []Image{}, fmt.Errorf("error getting images from repository: %w", err)
	}
	var outList []Image
	for _, imageDetail := range imageList {
		digest := *imageDetail.ImageDigest
		pushedAt := *imageDetail.ImagePushedAt
		tags := imageDetail.ImageTags
		data := Image{
			Digest:   digest,
			PushedAt: pushedAt,
			Tags:     tags,
		}
		outList = append(outList, data)
	}
	sort.Slice(outList, func(i, j int) bool {
		return outList[i].PushedAt.After(outList[j].PushedAt)
	})
	return outList, nil
}

func (img *Image) ApplyRule(rule policies.Rule, index int) bool {
	if img.Action != "" {
		return false
	}
Tags:
	switch rule.Selection.TagStatus {
	case "tagged":
		if !img.isTagged() {
			return false
		}
		if len(rule.Selection.TagPrefixList) == 0 {
			break Tags
		}
		for _, tagPrefix := range rule.Selection.TagPrefixList {
			if img.containsTagPrefix(tagPrefix) {
				break Tags
			}
		}
		return false
	case "untagged":
		if img.isTagged() {
			return false
		}
	}

	switch rule.Selection.CountType {
	case "daysSinceImagePushed":
		if time.Since(img.PushedAt) < (time.Duration(rule.Selection.CountNumber) * day) {
			return false
		}
	case "imageCountLessThan":
		if index >= rule.Selection.CountNumber {
			return false
		}
	case "imageCountMoreThan":
		if index < rule.Selection.CountNumber {
			return false
		}
	}

	img.Action = rule.Action
	return true
}

func (img Image) isTagged() bool {
	return len(img.Tags) > 0
}

func (img Image) isExpired() bool {
	return img.Action == "expire"
}

func (img Image) containsTagPrefix(prefix string) bool {
	for _, tag := range img.Tags {
		if strings.HasPrefix(*tag, prefix) {
			return true
		}
	}
	return false
}

func GetExpired(images []Image) []Image {
	var output []Image
	for _, image := range images {
		if image.isExpired() {
			output = append(output, image)
		}
	}
	return output
}
