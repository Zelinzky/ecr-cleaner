package policies

import (
	"ecr-cleaner/internal/io"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"gopkg.in/yaml.v2"
)

type Filter struct {
	TagStatus     string   `yaml:"tagStatus,omitempty" json:"tagStatus,omitempty"`
	TagPrefixList []string `yaml:"tagPrefixList,omitempty" json:"tagPrefixList,omitempty"`
	CountType     string   `yaml:"countType" json:"countType"`
	CountNumber   int      `yaml:"countNumber" json:"countNumber"`
}

type Rule struct {
	RulePriority int    `yaml:"rulePriority" json:"rulePriority"`
	Description  string `yaml:",omitempty" json:"description,omitempty"`
	Selection    Filter `yaml:",omitempty" json:"selection,omitempty"`
	Action       string `yaml:",omitempty" json:"action,omitempty"`
}

type Policy struct {
	Name         string   `json:"name"`
	Repositories []string `yaml:",omitempty" json:"repositories,omitempty"`
	Rules        []Rule   `yaml:",omitempty" json:"rules,omitempty"`
}

func GenerateExamplePolicy() Policy {
	return Policy{
		Name:         "exampleName",
		Repositories: []string{"repo-name1", "repo-name2", "..."},
		Rules: []Rule{{
			RulePriority: 0,
			Description:  "main priority rule",
			Selection: Filter{
				TagStatus:     "tagged | untagged",
				TagPrefixList: []string{"tag1", "tag2"},
				CountType:     "imageCountMoreThan | imageCountLessThan",
				CountNumber:   10,
			},
			Action: "keep",
		}, {
			RulePriority: 1,
			Description:  "second priority rule",
			Selection: Filter{
				TagStatus:   "any",
				CountType:   "daysSinceImagePushed",
				CountNumber: 30,
			},
			Action: "expire",
		}},
	}
}

func CheckPolicies(policies []Policy, verbose bool) error {
	names := make(map[string]struct{})
	repos := make(map[string]struct{})
	for _, policy := range policies {
		if policy.Name == "" {
			return fmt.Errorf("all policies should have a name")
		}

		if _, ok := names[policy.Name]; ok {
			return fmt.Errorf("all policy names should be unique, policy %s is duplicated", policy.Name)
		}
		if verbose {
			fmt.Printf("checking policy %s\n", policy.Name)
		}
		names[policy.Name] = struct{}{}

		if len(policy.Repositories) == 0 {
			return fmt.Errorf("all policies should apply to at least one repocitory, policy %s don't have any", policy.Name)
		}

		for _, repository := range policy.Repositories {
			if _, ok := repos[repository]; ok {
				return fmt.Errorf("only one policy should apply per repository, repository %s appears in multiple policies or duplicated in the same policy", repository)
			}
			repos[repository] = struct{}{}
		}

		priorities := make(map[int]struct{})
		for _, rule := range policy.Rules {
			if rule.Description == "" {
				return fmt.Errorf("all rules in policy %s should have a description", policy.Name)
			}

			if _, ok := priorities[rule.RulePriority]; ok {
				return fmt.Errorf("rule priorities in policy %s should be unique, rulePriority %d is duplicated", policy.Name, rule.RulePriority)
			}
			priorities[rule.RulePriority] = struct{}{}
			if verbose {
				fmt.Printf("\tchecking rule %d\t\t", rule.RulePriority)
			}

			switch rule.Action {
			case "keep", "expire":
			default:
				return fmt.Errorf("invalid action on rule %d: %s, in policy %s", rule.RulePriority, rule.Description, policy.Name)
			}

			switch rule.Selection.CountType {
			case "imageCountMoreThan", "imageCountLessThan", "daysSinceImagePushed":
			default:
				return fmt.Errorf("invalid countType value on rule %d: %s, in policy %s", rule.RulePriority, rule.Description, policy.Name)
			}

			if rule.Selection.CountNumber < 1 {
				return fmt.Errorf("invalid countNumber value on rule %d: %s, in policy %s. countNumber must be a positive integer", rule.RulePriority, rule.Description, policy.Name)
			}

			switch rule.Selection.TagStatus {
			case "tagged", "untagged", "any", "":
			default:
				return fmt.Errorf("invalid tagStatus value on rule %d: %s, in policy %s", rule.RulePriority, rule.Description, policy.Name)
			}
			if verbose {
				fmt.Println(io.Green("OK"))
			}
		}
		if verbose {
			fmt.Print(io.Green("policy ", policy.Name, "\t\tOK\n\n"))
		}
	}
	return nil
}

func ReadPoliciesFromDir(filePath string) ([]Policy, error) {
	if _, err := os.Stat(filePath); err != nil {
		return nil, fmt.Errorf("file or directory presented an error: %w", err)
	}

	var policyList []Policy
	err := filepath.Walk(filePath, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			var policiesInFile []Policy
			switch filepath.Ext(info.Name()) {
			case ".yaml", ".YAML", ".yml", ".YML":
				inBytes, err := ioutil.ReadFile(path)
				if err != nil {
					return fmt.Errorf("error reading file: %w", err)
				}
				err = yaml.Unmarshal(inBytes, &policiesInFile)
				if err != nil {
					return fmt.Errorf("error unmarshaling policies: %w", err)
				}
			case ".json", ".JSON":
				inBytes, err := ioutil.ReadFile(path)
				if err != nil {
					return fmt.Errorf("error reading file: %w", err)
				}
				err = json.Unmarshal(inBytes, &policiesInFile)
				if err != nil {
					return fmt.Errorf("error unmarshaling policies: %w", err)
				}
			default:
				fmt.Printf("File %s has an invalid or no extension, it will be ignored.\n", info.Name())
				return nil
			}
			policyList = append(policyList, policiesInFile...)
		}
		return nil
	})
	if err != nil {
		return nil, fmt.Errorf("error traversing the directory %s: %w", filePath, err)
	}
	return policyList, nil
}
