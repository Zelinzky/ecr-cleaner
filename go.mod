module ecr-cleaner

go 1.15

require (
	github.com/aws/aws-sdk-go v1.36.5
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.0
	gopkg.in/yaml.v2 v2.2.8
)
