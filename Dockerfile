FROM golang:1.15-alpine AS build

WORKDIR /src/
COPY ./ /src/
RUN go build -o /bin/ecr-cleaner

FROM scratch
COPY --from=build /bin/ecr-cleaner /bin/ecr-cleaner
ENTRYPOINT ["/bin/ecr-cleaner apply /policies -y"]