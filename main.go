package main

import (
	"log"

	"ecr-cleaner/internal/commands"
)

func main() {
	if err := commands.NewDefaultCommand().Execute(); err != nil {
		log.Fatal("Something went wrong.\nDetails:\n", err)
	}
}
