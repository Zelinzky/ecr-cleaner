# ecr-cleaner

Small utility to clean ecr unused images.
ecr-cleaner aims to allow user of ecr to implement better and complex policies to manage the expiration of the images in the repositories.